import axios from 'axios'

function getTokenFromLS() {
    if (typeof window !== 'undefined') {
        const token = localStorage.getItem('TOKEN')
        return token
    }
    return ""
}

const axiosClient = axios.create({
    baseURL: 'https://dummyjson.com/',
    headers: { 'Authorization': `Bearer ${getTokenFromLS()}` }
});;

export default axiosClient