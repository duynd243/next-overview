import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
const MemberListing = () => {
    const router = useRouter();
    return (
        <div>MemberListing


            <button 
            onClick={()=>{
                router.push('/member/phuong')
            }}
            className='block'>
                Go to Phuong member page (using router)
            </button>

            <Link className='block' href="/member/phuong">
                Go to Phuong member page
            </Link>
        </div>
    )
}

export default MemberListing