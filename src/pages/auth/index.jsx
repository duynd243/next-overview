import axiosClient from '@/libs/axiosClient'
import React, { useEffect } from 'react'
import {
    useQuery,
} from '@tanstack/react-query'
import { useRouter } from 'next/router'
const AuthPage = () => {
    const router = useRouter();
    const {
        data
    } = useQuery({
        queryKey: ['auth'],
        queryFn: async () => {
            const res = await axiosClient.get("/auth/RESOURCE")
            return res.data
        }
    })
    useEffect(()=>{
        if(typeof window !== "undefined" && !localStorage.getItem("TOKEN")){
           router.push("/login")
        }
    })
    return (
        <div>AuthPage

            <button className='block' onClick={()=>{
                localStorage.removeItem("TOKEN");

            }}>
                Logout
            </button>
        </div>
    )
}

export default AuthPage