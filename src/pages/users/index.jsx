import React, { useEffect, useState } from 'react'
import axiosClient from '@/libs/axiosClient'
import {
    useQuery,
} from '@tanstack/react-query'
const UserListing = () => {
    const pageSize = 10

    const {
        data: users,
        isLoading,
    } = useQuery({
        queryKey: ['users'],
        queryFn: async () => {
            const res = await axiosClient.get(`/users`, {
                params: {
                    limit: 3
                }
            })
            return res.data.users
        }
    })

    return (
        <div className='max-w-lg mx-auto'>
            <div>UserListing</div>
            {isLoading ? <div>Loading</div> : null}
            <div className='grid sm:grid-cols-2 lg:grid-cols-3 gap-3'>
                {users?.map((u, index) => {
                    const isFirst = index === 0
                    return <div key={u?.id}
                        className={`border p-4 ${isFirst ? "col-span-2" : ""}`}
                    >
                        {u?.firstName}
                    </div>
                })}
            </div>
        </div>
    )
}

export default UserListing