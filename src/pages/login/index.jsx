import axiosClient from '@/libs/axiosClient';
import { useRouter } from 'next/router';
import React, { useState } from 'react'
import toast, { Toaster } from 'react-hot-toast';
const LoginPage = () => {
    const router = useRouter();
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");

    const handleSubmit = (e) => {
        e.preventDefault();
        // call login api
        axiosClient.post("/auth/login", {
            username: userName,
            password,
        }).then(res => {
            toast.success("Login success");
            router.push("/auth")
            localStorage.setItem("TOKEN", res.data.token)
        }).catch(err => {
            toast.error(err?.response?.data?.message || "Something went wrong")
        })
    }
    return (
        <div>
            <div>LoginPage</div>
            <form onSubmit={handleSubmit} className='space-y-3'>
                <input className='border block' type='text'
                    value={userName}
                    onChange={e => setUserName(e.target.value)}
                />
                <input className='border block' type='password'
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
                <button type="submit">
                    Login
                </button>
            </form>
        </div>
    )
}

export default LoginPage