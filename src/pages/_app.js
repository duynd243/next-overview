import '@/styles/globals.css'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'
import {
  QueryClient,
  QueryClientProvider,
  useQuery,
} from '@tanstack/react-query'
import toast, { Toaster } from 'react-hot-toast';


const queryClient = new QueryClient()


export default function App({ Component, pageProps }) {
  return <QueryClientProvider client={queryClient}>
    <Component {...pageProps} />
    <Toaster 
      position='top-right'
    />
    <ReactQueryDevtools initialIsOpen={false} />
  </QueryClientProvider>
}
